import React, {Component} from 'react';

import {Grid, Icon} from 'semantic-ui-react';

class BoardTile extends Component {
    onClick() {
        this.props.clickFunc(this.props.name);
    }

    render() {
        let iconForPlayer = function (player) {
            if (!player) {
                return null;
            }
            let iconName = player === "x" ? "delete" : "thin circle";
            return <Icon name={iconName} size="huge"/>
        };
        return (
            <Grid.Column className="square" verticalAlign="middle" onClick={this.onClick.bind(this)}>
                {iconForPlayer(this.props.value)}
            </Grid.Column>
        )
    }
}

BoardTile.propType = {
    name: React.PropTypes.string.isRequired,
    value: React.PropTypes.string.isRequired,
    clickFunc: React.PropTypes.func.isRequired,
};

export default BoardTile;
