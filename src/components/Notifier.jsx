import React, {Component} from 'react';

import {Message, Icon} from 'semantic-ui-react';

class Notifier extends Component {
    render() {
        let {winner} = this.props;
        let message = "";
        let player = "";
        let iconForPlayer = function(player) {
            let iconName = player === "x" ? "delete" : "thin circle";
            if (winner && winner.includes("Cat")) {
                iconName = "ban"
            }
            return <Icon name={iconName} size="large" />
        };
        if (winner) {
            message = winner.includes("Cat") ? winner : "Winner!!!";
            player = this.props.winner;
        } else {
            message = "Up Next";
            player = this.props.nextUp;
        }

        return (
            <Message icon>
                {iconForPlayer(player)}
                {message}
            </Message>
        )
    }
}

Notifier.propType = {
    winner: React.PropTypes.string.isRequired,
    nextUp: React.PropTypes.string.isRequired,
};

export default Notifier;
