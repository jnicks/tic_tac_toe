import React, {Component} from 'react';
import {Grid, Container, Segment} from 'semantic-ui-react';

import BoardTile from './BoardTile.jsx';

class Board extends Component {
    render() {
        return (
            <Container>
                <Grid celled="internally" columns={3}>
                    <Grid.Row>
                        <BoardTile key="a1" name="a1" value={this.props.boardState.a1} {...this.props}/>
                        <BoardTile key="a2" name="a2" value={this.props.boardState.a2} {...this.props}/>
                        <BoardTile key="a3" name="a3" value={this.props.boardState.a3} {...this.props}/>
                    </Grid.Row>
                    <Grid.Row>
                        <BoardTile key="b4" name="b1" value={this.props.boardState.b1} {...this.props}/>
                        <BoardTile key="b5" name="b2" value={this.props.boardState.b2} {...this.props}/>
                        <BoardTile key="b6" name="b3" value={this.props.boardState.b3} {...this.props}/>
                    </Grid.Row>
                    <Grid.Row>
                        <BoardTile key="c1" name="c1" value={this.props.boardState.c1} {...this.props}/>
                        <BoardTile key="c2" name="c2" value={this.props.boardState.c2} {...this.props}/>
                        <BoardTile key="c3" name="c3" value={this.props.boardState.c3} {...this.props}/>
                    </Grid.Row>
                </Grid>
            </Container>
        )
    }
}

Board.propType = {
    boardState: React.PropTypes.object.isRequired,
    clickFunc: React.PropTypes.object.isRequired,
};

export default Board;
