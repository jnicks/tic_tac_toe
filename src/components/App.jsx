import React, {Component} from 'react';

import {Container, Header, Button} from 'semantic-ui-react';
import Board from './Board.jsx';
import Notifier from './Notifier.jsx';

class App extends Component {
    constructor(props) {
        super(props);
            this.defaultBoardState = {
                a1: null,
                a2: null,
                a3: null,
                b1: null,
                b2: null,
                b3: null,
                c1: null,
                c2: null,
                c3: null,
            };
        this.state = {winner: null, nextUp: "x", boardState: this.defaultBoardState};
    }

    tileClick(tile) {
        let {boardState, winner, nextUp} = this.state;
        if (boardState[tile] || winner) {
            return; // Tile is already owned, or game is over
        }
        boardState[tile] = nextUp;
        nextUp = nextUp === "x" ? "o" : "x";
        winner = this.findWinner();
        this.setState({boardState,nextUp,winner});
    }

    resetGame() {
        let {boardState} = this.state;
        Object.keys(boardState).forEach(k => {
            boardState[k] = null;
        });
        this.setState({boardState: boardState, winner: null, nextUp: "x"});
    }

    findWinner() {
        let b = this.state.boardState;
        let winningConditions = [
            [b.a1, b.a2, b.a3],
            [b.b1, b.b2, b.b3],
            [b.c1, b.c2, b.c3],
            [b.a1, b.b1, b.c1],
            [b.a2, b.b2, b.c2],
            [b.a3, b.b3, b.c3],
            [b.c1, b.b2, b.a3],
            [b.a1, b.b2, b.c3],
        ];
        let winner = null;
        winningConditions.forEach(cond => {
            if (cond[0] === cond[1] && cond[1] === cond[2]) {
                winner = cond[0]
            }
        });
        let nullCount = 0;
        let cats = false;
        if (!winner) {
            Object.keys(b).forEach(k => {
                if (!b[k]) {
                    nullCount += 1;
                }
            })
        }
        if (!winner && nullCount === 0) {
            return "Cat's Game";
        } else {
            return winner;
        }
    }

    render() {
        return (
            <Container className="app" textAlign="center" padded>
                <Header as="h1">
                    Tic-Tac-Toe
                </Header>
                <Notifier {...this.state} />
                <Board clickFunc={this.tileClick.bind(this)}
                       {...this.state} />
                <Button onClick={this.resetGame.bind(this)}>Reset</Button>
            </Container>
        )
    }
}

export default App;
